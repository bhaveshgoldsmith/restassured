package com.bhavesh.RestApi.AdminTestSuite;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class TS_01 {

	@Test(description = "Testing the web service whether success 200 has been received")
	@Description("Description : Testing the web service whether success 200 has been received")
	@Severity(SeverityLevel.CRITICAL)
	@Feature("Test Circuits")

	public void test_numberOfCuircuitsFor20172017_Season() {

		given().when().
		get("http://ergast.com/api/f1/2017/circuits.json").
		then().assertThat().statusCode(200).and()
				.body("MRData.CircuitTable.Circuits.circuitId", hasSize(20)).and()
				.header("content-length", equalTo("4551")).and().body("MRData.series", equalTo("f1"));

	}

}
